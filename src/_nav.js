import React from 'react'
import CIcon from '@coreui/icons-react'
import {
  cilGraph,
  cilMenu,
} from '@coreui/icons'
import { CNavGroup, CNavItem } from '@coreui/react'

const _nav = [
  {
    component: CNavItem,
    name: 'Menu 1',
    to: '/dashboard',
    icon: <CIcon icon={cilMenu} customClassName="nav-icon" />,
  },
  {
    component: CNavGroup,
    name: 'Menu 2',
    to: '/menu',
    icon: <CIcon icon={cilGraph} customClassName="nav-icon" />,
    items: [
      {
        component: CNavItem,
        name: 'Menu 2_1',
        to: '/menu_2_1',
      },
    ],
  },
]

export default _nav
