import React, { useEffect, useState } from 'react'
import { CCard, CCardBody, CCol, CCardHeader, CRow } from '@coreui/react'
import { CChartBar, CChartLine } from '@coreui/react-chartjs'
import {
  Circle,
  CircleMarker,
  MapContainer,
  Marker,
  Polygon,
  Polyline,
  Popup,
  Rectangle,
  TileLayer,
} from 'react-leaflet'
import 'leaflet/dist/leaflet.css'
import './MenuTwo.css'

const MenuTwo = () => {
  const center = [-6.525459, 107.179871]
  const positionJakarta = [-6.177078, 106.826935]
  const positionBandung = [-6.934947, 107.60473]

  const polygon = [
    [-6.177078, 106.826935],
    [-6.934947, 107.60473],
  ]

  const fillGreenOptions = { fillColor: 'green' }
  const fillYellowOptions = { fillColor: 'yellow' }
  const blueOptions = { color: 'blue' }
  const redOptions = { color: 'red' }

  const url = 'https://jsonplaceholder.typicode.com/posts'

  const [data, setData] = useState({
    totalPost: [],
    totalMember: [],
    totalTwoMember: [],
    dataTable: [],
  })

  const getDataUsers = async () => {
    const response = await fetch(url)
    const dataku = await response.json()
    const data = dataku.slice(0, 20)
    setData({
      ...data,
      dataTable: data,
      totalMember: dataku.reduce(
        (a, { userId, id }) => ((a[userId] = (a[userId] || 0) + +id), a),
        {},
      ),
      totalTwoMember: data.reduce(
        (a, { userId, id }) => ((a[userId] = (a[userId] || 0) + +id), a),
        {},
      ),
      totalPost: data.filter(
        (data, index, self) => index === self.findIndex((t) => t.userId === data.userId),
      ),
      // totalPost: JSON.stringify(data, (key, value) => {
      //   if (key === 'title' || key === 'body' || key === 'userId') {
      //     return
      //   }

      //   return value
      // }),
    })
  }

  console.log(
    Object.values(
      data.totalPost.reduce((a, { userId, id }) => ((a[id] = (a[id] || 0) + +userId), a), {}),
    ),
    'test coba',
  )

  const idTwoMember = Object.values(
    data.totalPost.reduce((a, { userId, id }) => ((a[id] = (a[id] || 0) + +userId), a), {}),
  )

  useEffect(() => {
    getDataUsers()
  }, [])

  console.log(Object.values(data.totalMember), 'ini member')

  return (
    <CRow>
      <CCol xs={12}>
        <CCard className="mb-4">
          <CCardHeader>Maps</CCardHeader>
          <CCardBody>
            <MapContainer center={center} zoom={10} scrollWheelZoom={true}>
              <TileLayer
                attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              />
              <Marker position={positionJakarta}>
              <Popup>
                This is Jakarta City.
              </Popup>
            </Marker>
            <Marker position={positionBandung}>
              <Popup>
                This is Bandung City.
              </Popup>
            </Marker>
              <Circle center={center} pathOptions={fillYellowOptions} radius={20} />
              <Circle center={positionJakarta} pathOptions={fillGreenOptions} radius={20} />
              <CircleMarker center={positionBandung} pathOptions={redOptions} radius={20}>
                <Popup>Bandung City</Popup>
              </CircleMarker>
              <Polygon pathOptions={blueOptions} positions={polygon} />
            </MapContainer>
            ,
          </CCardBody>
        </CCard>
      </CCol>
      <CCol xs={6}>
        <CCard className="mb-4">
          <CCardHeader>Bar Chart</CCardHeader>
          <CCardBody>
            <CChartBar
              data={{
                labels: Object.values(data.totalMember),
                datasets: [
                  {
                    label: 'All Member',
                    backgroundColor: '#f87979',
                    data: [40, 20, 12, 39, 10, 40, 39, 100, 40],
                  },
                ],
              }}
            />
          </CCardBody>
        </CCard>
      </CCol>
      <CCol xs={6}>
        <CCard className="mb-4">
          <CCardHeader>Line Chart</CCardHeader>
          <CCardBody>
            <CChartLine
              data={{
                labels: idTwoMember,
                datasets: [
                  {
                    label: 'Two Member',
                    backgroundColor: 'rgba(220, 220, 220, 0.2)',
                    borderColor: 'rgba(220, 220, 220, 1)',
                    pointBackgroundColor: 'rgba(220, 220, 220, 1)',
                    pointBorderColor: '#fff',
                    data: Object.values(data.totalTwoMember),
                  },
                ],
              }}
            />
          </CCardBody>
        </CCard>
      </CCol>
      <CCol xs={12}>
        <CCard className="mb-4">
          <table className="table">
            <thead>
              <tr>
                <th scope="col">User ID</th>
                <th scope="col">ID</th>
                <th scope="col">Title</th>
              </tr>
            </thead>
            <tbody>
              {data.dataTable.map((data) => {
                return (
                  <tr>
                    <th scope="row">{data.userId}</th>
                    <td>{data.id}</td>
                    <td>{data.title}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
        </CCard>
      </CCol>
    </CRow>
  )
}

export default MenuTwo
