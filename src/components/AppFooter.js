import React from 'react'
import { CFooter } from '@coreui/react'

const AppFooter = () => {
  return (
    <CFooter>
      <div>
        <a target="_blank" rel="noopener noreferrer">
          Created by
        </a>
        <span className="ms-1">&copy; 2022 Muhammad Ashi Dicky.</span>
      </div>
    </CFooter>
  )
}

export default React.memo(AppFooter)
